
var Courier = React.createClass({
  render: function(){
    return(
      <div className="courier">
        <h4>Courier</h4>
        <span className="label">Name:</span> {this.props.courier.name}
      </div>
    )
  }
});

var Customer = React.createClass({
  render: function(){
    return(
      <div className="customer">
        <h4>Customer</h4>
        <div className="name">
          <span className="label"> Name: </span>{this.props.customer.name}
        </div>
        <div className="address">
          <span className="label">Address: </span>
          {this.props.customer.address}
        </div>
      </div>
    );
  }
});

var Card = React.createClass({
  remove: function(){
    this.props.onRemove(this.props.index);
  },

  toHHMMSS: function( seconds ) {
    var sec_num = parseInt(seconds, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if ( seconds < 10 ) seconds = '0'+seconds;
    var time = seconds+'s';

    if ( minutes ) {
      if ( minutes < 10 ) minutes = "0"+minutes;
      time = minutes + ' m '+ time;
    }

    if ( hours ) {
      if ( hours < 10 ) hours = "0"+hours;
      time = hours+' h '+time;
    }
    return time;
  },
  render: function(){
    return(
      <div className="card">
        <h2>ORDER {this.props.uniqueId}</h2>
        <Customer customer={this.props.customer} />
        <Courier courier={this.props.courier} />

        <div className="eta">
          <span className="label">Pick Up ETA: </span>{this.toHHMMSS(this.props.eta)}
        </div>

        <button className="remove"
                onClick={this.remove}> Picked up </button>
      </div>
      );
  }
});

var Modal = React.createClass({
  cancel: function(){
    this.props.cancel();
  },
  clearExpediter: function(){
    this.props.clearExpediter();
  },
  render: function(){
    this.style = {
        display: this.props.open ? 'inherit' : 'none'
    };
    return (
      <div className="modal" style={this.style}>
        <p>Are you sure you want to close the kitchen?</p>
        <div className="confirm">
          <button className="confirm-ok" onClick={this.clearExpediter}> OK </button>
          <button className="cancel" onClick={this.cancel}>Cancel</button>
        </div>
      </div>
    )
  }

});

var Expediter = React.createClass({
  loadOrders: function(){
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var arr = this.state.orders;
        arr.push(data);

        arr.sort(function(a, b) {
          return a.pickup_eta - b.pickup_eta;
        });

        this.setState({orders: arr});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState:function(){
    return { orders: [], modalOpen: false };
  },
  componentDidMount: function() {
    var self = this;
    this.loadOrders();

    this.timer = setInterval(function(){
      self.counter = self.counter || 0;
      self.loadOrders();
      self.counter ++;

       if(self.counter >= self.props.totalOrders){
         clearInterval(self.timer);
      }
    }, 5000);

  },

  showModal: function(){
    this.setState({ modalOpen: true });
  },
  closeModal: function(){
    this.setState({ modalOpen: false });
  },
  clearExpediter: function(){
    this.closeModal();
    clearInterval(this.timer);
    this.setState({orders: []});
  },

  remove: function(i){
    var arr = this.state.orders;
    arr.splice(i, 1);
    this.setState({orders: arr});
  },

  eachOrder: function(order, i){
    return (
      <Card customer={order.customer}
      courier={order.courier}
      key={i}
      index={i}
      uniqueId={order.id}
      onRemove={this.remove}
      eta={order.pickup_eta} />
    );
  },

  render: function(){
    return(
      <div className='expediter'>
        <div className="row">
          <button className="help" onClick={this.showModal}> Help </button>
        </div>
        <Modal open={this.state.modalOpen}
          cancel={this.closeModal}
          clearExpediter={this.clearExpediter}/>

        <div className="orders">
          { this.state.orders.map(this.eachOrder)}
        </div>
      </div>
    )
  }
});

ReactDOM.render(
  <Expediter url='/orders' totalOrders={150}/>,
  document.getElementById('container')
);
