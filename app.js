
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var faker = require('faker');
var app = express();

app.set('port', (process.env.PORT || 3000));

app.use('/', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Cache-Control', 'no-cache');
    next();
});

function randomOrder(){
  return {
    id: faker.random.number(),
    customer:{
      name: faker.name.findName(),
      address:faker.address.streetAddress(),
    },
    courier:{
      name: faker.company.companyName()
    },
    pickup_eta: faker.random.number() // assume seconds
  }
}

app.get('/orders', function(req, res) {
  res.json(randomOrder())
});


module.exports = app;
